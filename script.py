input_file = "input.txt"
rows = 0
columns = 0
drones_num = 0
max_turnes = 0
max_payload = 0
products = []
warehouses = []
orders = []


class WareHouse:
    def __init__(self, x, y):
        self.location = (x, y)
        self.items = []


class Order:
    def __init__(self, location):
        self.location = location
        self.items_hash = {}


def parser():
    global input_file
    global rows
    global columns
    global drones_num
    global max_turnes
    global max_payload
    global products
    global warehouses
    global orders
    f = open(input_file, "r")
    content = f.readlines()
    print content
    rows = int(((content[0]).split(' '))[0])
    columns = int(((content[0]).split(' '))[1])
    drones_num = int(((content[0]).split(' '))[2])
    max_turnes = int(((content[0]).split(' '))[3])
    max_payload = int(((content[0]).split(' '))[4])

    print "first line: ", (content[0])

    products_num = int(content[1])
    print "products number: ", (content[0])
    for i in range(products_num):
        products.append(int(((content[2]).split(' '))[i]))

    cur_line = 3
    warehouses_num = int(content[cur_line])
    print "warehouse number: ", content[cur_line]
    for i in range(warehouses_num):
        cur_line += 1
        print "x, y ", str(i), " ", content[cur_line]
        x = int(((content[cur_line]).split(' '))[0])
        y = int(((content[cur_line]).split(' '))[1])
        wareh = WareHouse(x, y)
        cur_line += 1
        items = (content[cur_line])
        print "items: ", content[cur_line]
        for item in items.split(' '):
            wareh.items.append(int(item))
        warehouses.append(wareh)
    cur_line += 1

    orders_num = int(content[cur_line])
    print "orders number : ", content[cur_line]
    for i in range(orders_num):
        cur_line += 1
        location = (int(((content[cur_line]).split(' '))[0]), int(((content[cur_line]).split(' '))[1]))
        print "location: ", content[cur_line]
        cur_line += 2
        items = (content[cur_line])
        print "items: ", content[cur_line]
        o = Order(location)

        for j in range(products_num):
            o.items_hash[j] = 0
        for item in items.split():
            o.items_hash[int(item)] += 1
        orders.append(o)
        print o.items_hash

parser()
