import math

input_file = "input.txt"
rows = 0
columns = 0
drones_num = 0
max_turns = 0
max_payload = 0
products = []
warehouses = []
orders = []
drones = []


class WareHouse:
    def __init__(self, x, y):
        self.location = (x, y)
        self.items = []

class Drone:
    def __init__(self):
        self.list_of_items = []
        self.own_payload = max_payload
        self.available=0

class Order:
    def __init__(self, location):
        self.location = location
        self.items = []


def parser():
    global input_file
    global rows
    global columns
    global drones_num
    global max_turnes
    global max_payload
    global products
    global warehouses
    global orders
    f = open(input_file, "r")
    content = f.readlines()
    #print content
    rows = int(((content[0]).split(' '))[0])
    columns = int(((content[0]).split(' '))[1])
    drones_num = int(((content[0]).split(' '))[2])
    max_turnes = int(((content[0]).split(' '))[3])
    max_payload = int(((content[0]).split(' '))[4])

    #print "first line: ", (content[0])

    products_num = int(content[1])
    #print "products number: ", (content[0])
    for i in range(products_num):
        products.append(int(((content[2]).split(' '))[i]))

    cur_line = 3
    warehouses_num = int(content[cur_line])
    #print "warehouse number: ", content[cur_line]
    for i in range(warehouses_num):
        cur_line += 1
        #print "x, y ", str(i), " ", content[cur_line]
        x = int(((content[cur_line]).split(' '))[0])
        y = int(((content[cur_line]).split(' '))[1])
        wareh = WareHouse(x, y)
        cur_line += 1
        items = (content[cur_line])
        #print "items: ", content[cur_line]
        for item in items.split(' '):
            wareh.items.append(int(item))
        warehouses.append(wareh)
    cur_line += 1

    orders_num = int(content[cur_line])
    #print "orders number : ", content[cur_line]
    for i in range(orders_num):
        cur_line += 1
        location = (int(((content[cur_line]).split(' '))[0]), int(((content[cur_line]).split(' '))[1]))
        #print "location: ", content[cur_line]
        cur_line += 2
        items = (content[cur_line])
        #print "items: ", content[cur_line]
        o = Order(location)

        for j in range(products_num):
            o.items += [0]
        for item in items.split():
            o.items[int(item)] += 1
        orders.append(o)
        #print o.items


def findClosestWarehouse(order):
    ref = order.location
    warehousesByDist = sorted(warehouses,key=lambda x: (x[0] - ref[0]) ** 2 + (x[1] - ref[1]) ** 2)
    return warehousesByDist

def availableOfProduct(p,w):
    return

def findClosestDrone(w):
    ref = w.location
    dronesByDist = sorted(drones,key=lambda x: (x[0] - ref[0]) ** 2 + (x[1] - ref[1]) ** 2)
    return dronesByDist

def sortOrders(orders,w):
    ref = w.location
    sortedOrders = sorted(orders,key=lambda x: ((x.location[0] - ref[0]) ** 2 + (x.location[1] - ref[1]) ** 2))
    return sortedOrders
# 10000*sum(k > 0 for k in x.items) + sum([x.items[i]*products[i] for i in range(len(products))])

if __name__ == '__main__':
    parser()
    for x in range(drones_num):
        drones += [Drone()]
    drones_loc=[]
    for single_drone in range(drones_num):
        drones_loc +=[warehouses[0].location]
    t = -1
    while t < max_turns:

        t+=1
        k = -1
        for warehouse in warehouses:
            k+=1
            sorted_orders= sortOrders(orders,warehouse)
            for single_order in sorted_orders:
                j = -1
                for single_drone in drones:
                    j+=1
                    if single_drone.available > t:
                        continue
                    i = -1
                    while i < len(single_order.items):
                        i+=1
                        print i
                        if not(single_order.items[i]):
                            continue
                        minimum = int(min(single_order.items[i]*products[i], single_drone.own_payload)// products[i])
                        print str(j)+" L "+str(k)+" "+str(i)+" "+str(minimum)
                        single_drone.list_of_items += (products[i], minimum, i)
                        warehouse.items[i] -= minimum
                        single_order.items[i] -= minimum
                        for item in single_drone.list_of_items:
                            print str(j)," D ",str(item[0])," ",str(item[2])," ",str(item[1])
                            decx = single_order.location[0] - (drones_loc[j])[0]
                            decy = single_order.location[1] - (drones_loc[j])[1]
                            single_drone.available += math.hypot(decx, decy)

